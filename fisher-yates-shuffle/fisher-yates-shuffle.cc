#include <cstdlib>
#include <cstdio>
#include <cstdint>

#include <time.h>

static inline int rng() {
  return rand();
}

static inline int uniform(int max) {
  int r = 0;
  int limit = RAND_MAX - (RAND_MAX % max);
  for (r = rng(); r >= limit; r = rng())
    ;
  return r % max;
}

static inline void swap(char **a, char **b) {
  char *tmp = *a;
  *a = *b;
  *b = tmp;
}

static void shuffle(char **list, int length) {
  for (int i = length - 1; i > 0; --i) {
    int j = uniform(i + 1);
    swap(&list[i], &list[j]);
  }
}

int main(int argc, char *argv[]) {
  srand(time(NULL) + argc);
  shuffle(argv + 1, argc - 1);
  for (size_t i = 1; i < static_cast<size_t>(argc); ++i) {
    puts(argv[i]);
  }
  return 0;
}
