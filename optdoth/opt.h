/* SPDX-License-Identifier: ISC
 *
 * ISC License
 *
 * Copyright (c) 2016-2018 Christoph Göttschkes
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILIYT
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef OPT_H_INCLUDED_
#define OPT_H_INCLUDED_

/***********************************************************
 * Option parsing, used as follows:
 *
 *     int main(int argc, char *argv[]) {
 *       OPT_ERR_EXIT_CODE = 64; // is 1 by default
 *       OPT_START {
 *       case 'a':
 *         printf("OPTION A\n");
 *         break;
 *       case 'b':
 *       case 'c':
 *         if (!OPT_ARG) {
 *           OPT_ARG_REQ(); // calls exit
 *         }
 *         printf("OPTION %c with argument %s\n", OPT_OPT, OPT_ARG);
 *         break;
 *       case 'd': {
 *         int d_opt_arg;
 *         if (!OPT_ARG) {
 *           OPT_ARG_REQ(); // calls exit
 *         }
 *         if (sscanf(OPT_ARG, "%d", &d_opt_arg) != 1) {
 *           OPT_ILL_ARG("must be an integer"); // calls exit
 *         }
 *         printf("-d = %d\n", d_opt_arg);
 *         break;
 *       }
 *       default:
 *         OPT_ILL();
 *       } OPT_END
 *       while (argc--) {
 *         printf("ARGUMENT: %s\n", *(argv++));
 *       }
 *       return 0;
 *     }
 *
 * The arguments to `main` must be named `argc` and `argv`.
 *
 *     int main(int argc, char *argv[]);
 *
 * - Wrap the body of the switch statement in OPT_START and OPT_END.
 * - Inside the body, use OPT_OPT to access the current option character.
 * - Inside the body, use OPT_ARG to access the argument of the current option
 *   (if it should have one).
 * - OPT_ILL, OPT_ILL_ARG, and OPT_ARG_REQ are helpers, which print out a well
 *   known error message.
 * - PROG_NAME references the program name with which the executable has been
 *   called. Store it (after the OPT_END) in a global variable, if it needs to
 *   be used by a different compilation unit.
 * - ARG_REQ is a helper which prints a well known error message,
 *   if a mandatory argument (not an option) is missing.
 *
 * After OPT_END, `argv` will point to the first argument (first element which
 * is not an option) and `argc` stores the number of arguments available.
 *
 * Using OPT_STOP stops the argument processing immediately and jumps to OPT_END.
 * `argv[0]` will point to the position of the current option, or argument of
 * the option, depending if OPT_ARG has been used or not. `argc` contains the
 * number of argv remaining. Use OPT_STOP with caution.
 * Better use OPT_ILL() / OPT_ARG_REQ() / OPT_ILL_ARG(msg) and exit if an error
 * occurs.
 *
 * OPT_ILL, OPT_ARG_REQ, OPT_ILL_ARG, and ARG_REQ exit automatically after they
 * are called, iff OPT_ERR_EXIT_CODE is >= 0 (the default value is 1).
 *
 * The implementation supports:
 * - packed options
 *     `-h -v == -hv == -vh == -v -h`
 * - arguments directly following the option
 *     `-a foo == -afoo`
 * - preceding boolean options before an option with an argument
 *     `-h -a foo == -hafoo == -h -afoo`
 * - the argument `--` stops the processing of options
 *
 * The GNU argv permutation extension is not supported.
 * The GNU long options extension is not supported.
 **********************************************************/

/***********************************************************
 * Version history
 *
 *  015   Clarified the documentation.
 *  014   Adds SPDX license identifier: https://spdx.org/ids
 *  013   Removes { for the switch in OPT_START and first } in OPT_END
 *        to force the user to specify the {}
 *  012   Introduces OPT_ILL_ARG(msg)
 *  011   Introduces OPT_ERR_EXIT_CODE and exit after error reporting
 *        automatically, iff OPT_ERR_EXIT_CODE >= 0
 *  010   Introduces ARG_REQ()
 *  009   Fixes expansion problems in OPT_START and OPT_END
 *  008   Fixes expansion problems in OPT_ARG
 *  007   Introduces OPT_STOP()
 *  006   Introduces OPT_ILL() and _OPT_ARG_REQ()
 *  005   Adds documentation
 *  004   Fixes OPT_ARG evaluation
 *  003   Identifiers declared by the macros now end with a _ to avoid clashes
 *  002   OPT_ARG can now be used multiple times in one case
 *  001   OPT_START, OPT_END, OPT_ARG and OPT_OPT, PROG_NAME
 **********************************************************/

static char *opt_global_prog_name_ = "";
static int opt_global_exit_code_ = 1;

#define OPT_START											\
	(void) (opt_global_exit_code_); /* -Wunused-variable */					\
	if (argc > 0) {										\
		opt_global_prog_name_ = argv[0];							\
		--argc, ++argv;									\
	}												\
	/* iterate over argv */									\
	for (/* NOP */; argc && argv[0][0] == '-' && argv[0][1] != '\0'; --argc, ++argv) {		\
		char *opt_optarg_ = NULL;								\
		/* handle special '--' argument, stop processing */					\
		if (argv[0][1] == '-' && argv[0][2] == '\0') {						\
			--argc, ++argv;								\
			goto opt_stop_label_;								\
	        }											\
		/* iterate packed options */								\
		for (++argv[0]; !opt_optarg_ && argv[0][0]; ++argv[0]) {				\
			char opt_optopt_ = argv[0][0];							\
			switch (opt_optopt_)

#define OPT_END											\
	}                     /* inner for */								\
	}                     /* outer for */								\
	goto opt_stop_label_; /* -Wunused-label */							\
	opt_stop_label_:;

#define OPT_STOP() goto opt_stop_label_

#define OPT_ARG											\
	(opt_optarg_ ? opt_optarg_									\
	 : (opt_optarg_ = (argv[0][1] != '\0')								\
	                  ? (argv[0]++, &argv[0][0])							\
	                  : ((argc <= 1) ? (char *) NULL						\
	                     : (--argc, ++argv, argv[0])),						\
	    opt_optarg_))

#define OPT_OPT opt_optopt_

#define OPT_ILL()											\
	do {												\
		fprintf(stderr, "%s: illegal option -- %c\n", PROG_NAME, OPT_OPT);			\
		if (opt_global_exit_code_ >= 0)							\
			exit(opt_global_exit_code_);							\
	} while (0)

#define OPT_ILL_ARG(msg)										\
	do {												\
		fprintf(stderr, "%s: illegal argument to -%c -- %s\n", PROG_NAME, OPT_OPT, msg);	\
		if (opt_global_exit_code_ >= 0)							\
			exit(opt_global_exit_code_);							\
	} while (0)

#define OPT_ARG_REQ()											\
	do {												\
		fprintf(stderr, "%s: option requires an argument -- %c\n", PROG_NAME, OPT_OPT);	\
		if (opt_global_exit_code_ >= 0)							\
			exit(opt_global_exit_code_);							\
	} while (0)

#define ARG_REQ(arg)											\
	do {												\
		fprintf(stderr, "%s: argument required -- %s\n", PROG_NAME, arg);			\
		if (opt_global_exit_code_ >= 0)							\
			exit(opt_global_exit_code_);							\
	} while (0)

#define OPT_ERR_EXIT_CODE opt_global_exit_code_

#define PROG_NAME opt_global_prog_name_

#endif /* OPT_H_INCLUDED_ */
