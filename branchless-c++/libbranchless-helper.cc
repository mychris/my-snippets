#include <stdlib.h>

#include "branchless-helper.h"

extern "C" bool constant_true()
{
  return true;
}

extern "C" bool constant_false()
{
  return false;
}

extern "C" bool alternating_true_false()
{
  static bool v = true;
  bool result = v;
  v = !v;
  return result;
}

extern "C" bool random_true_false()
{
  return rand() & 1;
}

extern "C" void swallow_int(int x)
{
  (void) x;
}
