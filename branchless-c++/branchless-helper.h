
extern "C" {

bool constant_true();

bool constant_false();

bool alternating_true_false();

bool random_true_false();

void swallow_int(int);

}
