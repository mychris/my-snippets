#include <stddef.h>
#include <stdlib.h>

#include "branchless-helper.h"
#include "benchmark/benchmark.h"

typedef bool (*BoolGen)();

template <BoolGen B>
void bench_branch_unoptimized_prediction(benchmark::State &state)
{
  srand(1);
  const size_t array_size = static_cast<size_t>(state.range(0));
  unsigned int *a1 = new unsigned int[array_size];
  unsigned int *a2 = new unsigned int[array_size];
  bool *b = new bool[array_size];
  for (size_t i = 0; i < array_size; i++) {
    a1[i] = static_cast<unsigned int>(rand());
    a2[i] = static_cast<unsigned int>(rand());
    b[i] = B();
  }
  for (auto _ : state) {
    unsigned int sum = 0;
    for (size_t i = 0; i < array_size; i++) {
      if (b[i]) {
        sum += a1[i];
      } else {
        sum += a2[i];
      }
    }
    benchmark::DoNotOptimize(sum);
    benchmark::ClobberMemory();
  }
  state.SetItemsProcessed(static_cast<long>(array_size) * state.iterations());
  delete[] a1;
  delete[] a2;
  delete[] b;
}

BENCHMARK(bench_branch_unoptimized_prediction<constant_true>)->Arg(1 << 23);
BENCHMARK(bench_branch_unoptimized_prediction<alternating_true_false>)->Arg(1 << 23);
BENCHMARK(bench_branch_unoptimized_prediction<random_true_false>)->Arg(1 << 23);

template <BoolGen B>
void bench_branch_prediction_optimized_mult(benchmark::State &state)
{
  srand(1);
  const size_t array_size = static_cast<size_t>(state.range(0));
  unsigned int *a1 = new unsigned int[array_size];
  unsigned int *a2 = new unsigned int[array_size];
  bool *b = new bool[array_size];
  for (size_t i = 0; i < array_size; i++) {
    a1[i] = static_cast<unsigned int>(rand());
    a2[i] = static_cast<unsigned int>(rand());
    b[i] = B();
  }
  for (auto _ : state) {
    unsigned int sum = 0;
    for (size_t i = 0; i < array_size; i++) {
      sum += static_cast<unsigned int>(b[i]) * a1[i];
      sum += static_cast<unsigned int>(!b[i]) * a2[i];
    }
    benchmark::DoNotOptimize(sum);
    benchmark::ClobberMemory();
  }
  state.SetItemsProcessed(static_cast<long>(array_size) * state.iterations());
  delete[] a1;
  delete[] a2;
  delete[] b;
}

BENCHMARK(bench_branch_prediction_optimized_mult<constant_true>)->Arg(1 << 23);
BENCHMARK(bench_branch_prediction_optimized_mult<alternating_true_false>)->Arg(1 << 23);
BENCHMARK(bench_branch_prediction_optimized_mult<random_true_false>)->Arg(1 << 23);

template <BoolGen B>
void bench_branch_prediction_optimized_arr(benchmark::State &state)
{
  srand(1);
  const size_t array_size = static_cast<size_t>(state.range(0));
  unsigned int *a1 = new unsigned int[array_size];
  unsigned int *a2 = new unsigned int[array_size];
  bool *b = new bool[array_size];
  for (size_t i = 0; i < array_size; i++) {
    a1[i] = static_cast<unsigned int>(rand());
    a2[i] = static_cast<unsigned int>(rand());
    b[i] = B();
  }
  for (auto _ : state) {
    unsigned int sum = 0;
    for (size_t i = 0; i < array_size; i++) {
      const unsigned int p[] = { a2[i], a1[i] };
      sum += p[static_cast<unsigned int>(b[i])];
    }
    benchmark::DoNotOptimize(sum);
    benchmark::ClobberMemory();
  }
  state.SetItemsProcessed(static_cast<long>(array_size) * state.iterations());
  delete[] a1;
  delete[] a2;
  delete[] b;
}

BENCHMARK(bench_branch_prediction_optimized_arr<constant_true>)->Arg(1 << 23);
BENCHMARK(bench_branch_prediction_optimized_arr<alternating_true_false>)->Arg(1 << 23);
BENCHMARK(bench_branch_prediction_optimized_arr<random_true_false>)->Arg(1 << 23);

BENCHMARK_MAIN();
