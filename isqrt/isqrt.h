#include <stdint.h>

uint8_t
isqrtu8(uint8_t);

uint16_t
isqrtu16(uint16_t);

uint32_t
isqrtu32(uint32_t);

uint64_t
isqrtu64(uint64_t);

int8_t
isqrts8(int8_t);

int16_t
isqrts16(int16_t);

int32_t
isqrts32(int32_t);

int64_t
isqrts64(int64_t);
