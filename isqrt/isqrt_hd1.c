#include <stdint.h>

#include "isqrt.h"

uint8_t
isqrtu8(uint8_t x)
{
	uint8_t m, y, b;
	m = UINT8_C(0x40);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

uint16_t
isqrtu16(uint16_t x)
{
	uint16_t m, y, b;
	m = UINT16_C(0x4000);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

uint32_t
isqrtu32(uint32_t x)
{
	uint32_t m, y, b;
	m = UINT32_C(0x40000000);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

uint64_t
isqrtu64(uint64_t x)
{
	uint64_t m, y, b;
	m = UINT64_C(0x4000000000000000);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

int8_t
isqrts8(const int8_t x)
{
	if (x < 0)
		return -1;
	return (int8_t)isqrtu8((uint8_t) x);
}

int16_t
isqrts16(const int16_t x)
{
	if (x < 0)
		return -1;
	return (int16_t)isqrtu16((uint16_t) x);
}

int32_t
isqrts32(const int32_t x)
{
	if (x < 0)
		return -1;
	return (int32_t)isqrtu32((uint32_t) x);
}

int64_t
isqrts64(const int64_t x)
{
	if (x < 0)
		return -1;
	return (int64_t)isqrtu64((uint64_t) x);
}
