#include <stdint.h>

#include "isqrt.h"

uint8_t
isqrtu8(const uint8_t x)
{
	if (x == 0 || x == 1)
		return x;
	uint8_t i = 1, result = 1;
	while (i <= 15 && result <= x) {
		i++;
		result = i * i;
	}
	return i - 1;
}

uint16_t
isqrtu16(const uint16_t x)
{
	if (x == 0 || x == 1)
		return x;
	uint16_t i = 1, result = 1;
	while (i <= 255 && result <= x) {
		i++;
		result = i * i;
	}
	return i - 1;
}

uint32_t
isqrtu32(const uint32_t x)
{
	if (x == 0 || x == 1)
		return x;
	uint32_t i = 1, result = 1;
	while (i <= 65535 && result <= x) {
		i++;
		result = i * i;
	}
	return i - 1;
}

uint64_t
isqrtu64(const uint64_t x)
{
	if (x == 0 || x == 1)
		return x;
	uint64_t i = 1, result = 1;
	while (i <= 4294967296 && result <= x) {
		i++;
		result = i * i;
	}
	return i - 1;
}

int8_t
isqrts8(const int8_t x)
{
	if (x < 0)
		return -1;
	return (int8_t)isqrtu8((uint8_t) x);
}

int16_t
isqrts16(const int16_t x)
{
	if (x < 0)
		return -1;
	return (int16_t)isqrtu16((uint16_t) x);
}

int32_t
isqrts32(const int32_t x)
{
	if (x < 0)
		return -1;
	return (int32_t)isqrtu32((uint32_t) x);
}

int64_t
isqrts64(const int64_t x)
{
	if (x < 0)
		return -1;
	return (int64_t)isqrtu64((uint64_t) x);
}
