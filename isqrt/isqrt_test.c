#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "isqrt.h"

static int
test_isqrtu8(void)
{
	int result = 0;
	for (uint8_t x = 1; x != 0; ++x) {
		double real_sqrt = sqrt((double) x);
		uint8_t expected = (uint8_t) floor(real_sqrt);
		uint8_t actual = isqrtu8(x);
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrtu8 isqrtu8(%" PRIu8 ") -> %" PRIu8 " != %" PRIu8 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrts8(void)
{
	int result = 0;
	for (int8_t x = INT8_MIN; x != INT8_MAX; ++x) {
		int8_t actual, expected;
		if (x < 0) {
			expected = -1;
			actual = isqrts8(x);
		} else {
			double real_sqrt = sqrt((double) x);
			expected = (int8_t) floor(real_sqrt);
			actual = isqrts8(x);
		}
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrts8 isqrts8(%" PRId8 ") -> %" PRId8 " != %" PRId8 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}


static int
test_isqrtu16(void)
{
	int result = 0;
	for (uint16_t x = 1; x != 0; ++x) {
		double real_sqrt = sqrt((double) x);
		uint16_t expected = (uint16_t) floor(real_sqrt);
		uint16_t actual = isqrtu16(x);
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrtu16 isqrtu16(%" PRIu16 ") -> %" PRIu16 " != %" PRIu16 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrts16(void)
{
	int result = 0;
	for (int16_t x = INT16_MIN; x != INT16_MAX; ++x) {
		int16_t actual, expected;
		if (x < 0) {
			expected = -1;
			actual = isqrts16(x);
		} else {
			double real_sqrt = sqrt((double) x);
			expected = (int16_t) floor(real_sqrt);
			actual = isqrts16(x);
		}
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrts16 isqrts16(%" PRId16 ") -> %" PRId16 " != %" PRId16 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrtu32(void)
{
	int result = 0;
	const size_t rounds = 10000;
	for (size_t round = 0; round < rounds; ++round) {
		/* int has a min bit width of 16 */
		uint32_t x = (((uint32_t) rand() & UINT32_C(0xFFFF)) << 16)
		             | ((uint32_t) rand() & UINT32_C(0xFFFF));
		double real_sqrt = sqrt((double) x);
		uint32_t expected = (uint32_t) floor(real_sqrt);
		uint32_t actual = isqrtu32(x);
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrtu32 isqrtu32(%" PRIu32 ") -> %" PRIu32 " != %" PRIu32 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrts32(void)
{
	int result = 0;
	const size_t rounds = 10000;
	for (size_t round = 0; round < rounds; ++round) {
		int32_t actual, expected;
		/* int has a min bit width of 16 */
		int32_t x = (((int32_t) rand() & INT32_C(0xFFFF)) << 16)
		            | ((int32_t) rand() & INT32_C(0xFFFF));
		if (x < 0) {
			expected = (int32_t) -1;
			actual = isqrts32(x);
		} else {
			double real_sqrt = sqrt((double) x);
			expected = (int32_t) floor(real_sqrt);
			actual = isqrts32(x);
		}
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrts32 isqrts32(%" PRId32 ") -> %" PRId32 " != %" PRId32 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrtu64(void)
{
	int result = 0;
	const size_t rounds = 10000;
	for (size_t round = 0; round < rounds; ++round) {
		/* int has a min bit width of 16 */
		uint64_t x = (((uint64_t) rand() & UINT64_C(0xFFFF)) << 24)
		             | (((uint64_t) rand() & UINT64_C(0xFFFF)) << 16)
		             | (((uint64_t) rand() & UINT64_C(0xFFFF)) << 8)
		             | ((uint64_t) rand() & UINT64_C(0xFFFF));
		long double real_sqrt = sqrtl((long double) x);
		uint64_t expected = (uint64_t) floorl(real_sqrt);
		uint64_t actual = isqrtu64(x);
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrtu64 isqrtu64(%" PRIu64 ") -> %" PRIu64 " != %" PRIu64 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

static int
test_isqrts64(void)
{
	int result = 0;
	const size_t rounds = 10000;
	for (size_t round = 0; round < rounds; ++round) {
		int64_t actual, expected;
		/* int has a min bit width of 16 */
		int64_t x = (((int64_t) rand() & INT64_C(0xFFFF)) << 24)
		            | (((int64_t) rand() & INT64_C(0xFFFF)) << 16)
		            | (((int64_t) rand() & INT64_C(0xFFFF)) << 8)
		            | ((int64_t) rand() & INT64_C(0xFFFF));
		if (x < 0) {
			expected = -1;
			actual = isqrts64(x);
		} else {
			long double real_sqrt = sqrtl((long double) x);
			expected = (int64_t) floorl(real_sqrt);
			actual = isqrts64(x);
		}
		if (expected != actual) {
			fprintf(stderr,
			        "FAIL: test_isqrts64 isqrts64(%" PRId64 ") -> %" PRId64 " != %" PRId64 "\n",
			        x, actual, expected);
			result = -1;
		}
	}
	return result;
}

int
main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	int result = 0;
	result += test_isqrtu8();
	result += test_isqrts8();
	result += test_isqrtu16();
	result += test_isqrts16();
	result += test_isqrtu32();
	result += test_isqrts32();
	result += test_isqrtu64();
	result += test_isqrts64();
	return abs(result);
}
