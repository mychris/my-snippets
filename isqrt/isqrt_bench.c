#include <sys/time.h>

#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "isqrt.h"

volatile uint64_t black_whole = 0;

static uint8_t u8_nums[20000] = {0};
static uint16_t u16_nums[20000] = {0};
static uint32_t u32_nums[20000] = {0};
static uint64_t u64_nums[20000] = {0};

typedef uint64_t (*bench_func)(void);

static uint64_t
bench_round_u8(void)
{
	uint8_t result = 0;
	for (size_t i = 0; i < sizeof(u8_nums) / sizeof(u8_nums[0]); ++i) {
		result += isqrtu8(u8_nums[i]);
	}
	return (uint64_t) result;
}

static uint64_t
bench_round_u16(void)
{
	uint16_t result = 0;
	for (size_t i = 0; i < sizeof(u16_nums) / sizeof(u16_nums[0]); ++i) {
		result += isqrtu16(u16_nums[i]);
	}
	return (uint64_t) result;
}

static uint64_t
bench_round_u32(void)
{
	uint32_t result = 0;
	for (size_t i = 0; i < sizeof(u32_nums) / sizeof(u32_nums[0]); ++i) {
		result += isqrtu32(u32_nums[i]);
	}
	return (uint64_t) result;
}

static uint64_t
bench_round_u64(void)
{
	uint64_t result = 0;
	for (size_t i = 0; i < sizeof(u64_nums) / sizeof(u64_nums[0]); ++i) {
		result += isqrtu64(u64_nums[i]);
	}
	return (uint64_t) result;
}

static uint64_t
bench(const uint64_t rounds, bench_func b)
{
	uint64_t nanos = 0;
	uint64_t seconds = 0;
	struct timespec start, stop;
	for (uint64_t round = 0; round < rounds; ++round) {
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
		black_whole += b();
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
		nanos += (uint64_t)(stop.tv_sec - start.tv_sec) * UINT64_C(1000000000) +
		         (uint64_t)(stop.tv_nsec - start.tv_nsec);
		seconds += (nanos / UINT64_C(1000000000));
		nanos = nanos % UINT64_C(1000000000);
	}
	return seconds * 1000 + nanos / UINT64_C(1000000);
}

static void
fill_bench_data(void)
{
	for (size_t i = 0; i < sizeof(u8_nums) / sizeof(u8_nums[0]); ++i) {
		u8_nums[i] = (uint8_t) (rand() & UINT8_C(0xff));
	}
	for (size_t i = 0; i < sizeof(u16_nums) / sizeof(u16_nums[0]); ++i) {
		u16_nums[i] = (uint8_t) (rand() & UINT16_C(0xffff));
	}
	u32_nums[0] = UINT32_MAX;
	for (size_t i = 1; i < sizeof(u32_nums) / sizeof(u32_nums[0]); ++i) {
		u32_nums[i] = (((uint32_t) rand() & UINT32_C(0xFFFF)) << 16)
		              | ((uint32_t) rand() & UINT32_C(0xFFFF));
	}
	u64_nums[0] = UINT64_MAX;
	for (size_t i = 1; i < sizeof(u64_nums) / sizeof(u64_nums[0]); ++i) {
		u64_nums[i] = (((uint64_t) rand() & UINT64_C(0xFFFF)) << 24)
		              | (((uint64_t) rand() & UINT64_C(0xFFFF)) << 16)
		              | (((uint64_t) rand() & UINT64_C(0xFFFF)) << 8)
		              | ((uint64_t) rand() & UINT64_C(0xFFFF));
	}
}

int
main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	fill_bench_data();
	const uint64_t rounds = 500;
	uint64_t millis;
	millis = bench(rounds, bench_round_u8);
	printf("bench_u8 : %8" PRIu64 " rounds, %8zu numbers each took %" PRIu64
	       " ms.\n", rounds, sizeof(u8_nums) / sizeof(u8_nums[0]), millis);
	printf("   =>      %8" PRIu64 " round                         took %.2Lf ms.\n",
	       UINT64_C(1),
	       (long double) millis / (long double) rounds);

	millis = bench(rounds, bench_round_u16);
	printf("bench_u16: %8" PRIu64 " rounds, %8zu numbers each took %" PRIu64
	       " ms.\n", rounds, sizeof(u16_nums) / sizeof(u16_nums[0]), millis);
	printf("   =>      %8" PRIu64 " round                         took %.2Lf ms.\n",
	       UINT64_C(1),
	       (long double) millis / (long double) rounds);

	millis = bench(rounds, bench_round_u32);
	printf("bench_u32: %8" PRIu64 " rounds, %8zu numbers each took %" PRIu64
	       " ms.\n", rounds, sizeof(u32_nums) / sizeof(u32_nums[0]), millis);
	printf("   =>      %8" PRIu64 " round                         took %.2Lf ms.\n",
	       UINT64_C(1),
	       (long double) millis / (long double) rounds);

	millis = bench(rounds, bench_round_u64);
	printf("bench_u64: %8" PRIu64 " rounds, %8zu numbers each took %" PRIu64
	       " ms.\n", rounds, sizeof(u64_nums) / sizeof(u64_nums[0]), millis);
	printf("   =>      %8" PRIu64 " round                         took %.2Lf ms.\n",
	       UINT64_C(1),
	       (long double) millis / (long double) rounds);
	return 0;
}
