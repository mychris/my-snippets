# fasm-linux

Some small example programs wirtten in [fasm](https://flatassembler.net/) for
linux-amd64.

## License

Copyright (c) 2023 Christoph Göttschkes

Licensed under the ISC License
