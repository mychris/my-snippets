;; SPDX-License-Identifier: ISC

format ELF64 executable 3
entry _start

define STDOUT 1

define SYS_write 1
define SYS_exit 60

segment readable executable

_start:
	mov	rax, SYS_write		; SYS_write(
	mov	rdi, STDOUT		; 	unsigned int FD
	lea	rsi, [msg]		;	char *buf
	mov	rdx, msg.sz		;	size_t count
	syscall				; )
	mov	rax, SYS_exit		; SYS_exit(
	mov	rdi, 0x0		;	int error_code
	syscall				; )

segment readable

msg	db 'Hello World!', 10, 0
msg.sz	= $ - msg - 1
