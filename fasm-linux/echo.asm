;; SPDX-License-Identifier: ISC

format ELF64 executable 3
entry _start

define STDOUT 1

define SYS_write 1
define SYS_exit 60

segment readable executable

_start:
	pop	rcx			; argc
	cmp	rcx, 0x0		; should never be zero, but check
	jz	_END
	pop	r13			; pop the program name/path
	dec	rcx			; argc -= 1
	jmp	_LOOP_A
_LOOP_B:
	mov	r14, rcx
	call	print_spc		; print the space between
	mov	rcx, r14
_LOOP_A:
	pop	r13			; pop the next argv
	mov	r14, rcx		; save rcx across the upcoming calls
	mov	rdi, r13		; ( char*
	call	strlen			; ) strlen -> rax
	mov	rdi, r13		; ( char*
	mov	rsi, rax		;   size_t
	call	print			; ) print
	mov	rcx, r14
	loop	_LOOP_B			; decrement rcx, jmp to _LOOP if not zero
	call	println
_END:
	mov	rdi, 0x0		; ( int
	call	exit			; ) exit

;; size_t strlen(char *)
;;
;; Arguments
;; 	rdi	string address
;;
;; Returns
;; 	rax	length of the string
strlen:
	mov 	rcx, -1  		; move -1 to rcx
	xor 	al, al 			; set al to 0
	cld 				; clear the direction flags
	repne 	scasb 			; repeat if not equal to al
	neg 	rcx			; rcx = -rcx
	sub 	rcx, 2			; rcx -= 2
	mov 	rax, rcx		; set return value
	ret

;; void print(char *, size_t)
;; Arguments
;;	rdi	string address
;;	rsi	length of the string
print:
	mov	rdx, rsi
	mov	rsi, rdi
	mov	rax, SYS_write		; SYS_write(
	mov	rdi, STDOUT		;	unsigned int FD
	mov	rsi, rsi		;	char *buf
	mov	rdx, rdx		;	size_t count
	syscall				; )
	ret

;; void println()
println:
	lea	rdi, [nl]
	mov	rsi, 0x1
	call	print
	ret

;; void print_spc()
print_spc:
	lea	rdi, [spc]
	mov	rsi, 0x1
	call	print
	ret

;; void exit(int)
;; Arguments
;;	rdi	exit code
exit:
	mov	rax, SYS_exit		; SYS_exit(
	mov	rdi, rdi		;	int error_code
	syscall				; )

segment readable

nl	db 10, 0
spc	db ' ', 0
